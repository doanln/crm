import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './cats/cats.controller';
import { ShopeeController } from './shopee/shopee.controller';

@Module({
  imports: [],
  controllers: [AppController, CatsController, ShopeeController],
  providers: [AppService],
})
export class AppModule {}
