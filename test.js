var express = require('express');
var app = express();
// Includes crypto module
const crypto = require('crypto');

function getShopeeLoginUrl() {
    var timest = new Date().getTime();
    var host = "https://partner.test-stable.shopeemobile.com"
    var path = "/api/v2/shop/auth_partner"
    var redirect_url = "http://lottery.vcc.vn/test"
    var partner_id = '846367'
    var partner_key = '903eb938d82fda863001a3ba883675e9bc42913767e39e36bb2ffd37329698ed'
    var base_string = partner_id + path + timest

    var sign = crypto.createHmac('sha256', partner_key)
        // updating data
        .update(base_string)
        // Encoding to be used
        .digest('hex');
    // # autgenerate api
    var url = host + path + "?partner_id=" + partner_id + "&timestamp=" + timest + "&sign=" + sign + "&redirect=" + redirect_url
    return url;

}




app.get('/', async (req, res) => {
    res.send("<h3>Hello World</H3>")
});

app.get('/login', async (req, res) => {
    return res.redirect(getShopeeLoginUrl());
});


app.listen(3000, () => {
    console.log('Example app listening on port 3000!')
});

